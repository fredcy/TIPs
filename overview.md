## Tezos Improvement Proposals

Tezos Improvement Proposals (TIPs) describe standards for the Tezos platform, including core protocol specifications, client APIs, and contract standards.

A list of all TIPs and their names can be seen below.

Number | Title | Author 
---|---|---
1 | [TIP Purpose and Guidelines](TIPS/tip-1.md) | [Martin Pospěch](mailto:martin@pospech.cz)
7 | [TRC-7 Tezos ICO Standard](TIPS/tip-7.md) | [Martin Pospěch](mailto:martin@pospech.cz)