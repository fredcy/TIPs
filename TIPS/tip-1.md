---
tip: 1
title: TIP Purpose and Guidelines
status: Active
type: Meta
author: Martin Pospěch <martin@pospech.cz>
created: 2018-08-15
---

## What is a TIP?

TIP stands for Tezos Improvement Proposal. A TIP is a design document providing information to the Tezos community, or describing a new feature for Tezos or its processes or environment. The TIP should provide a concise technical specification of the feature and a rationale for the feature. The TIP author is responsible for building consensus within the community and documenting dissenting opinions.

## TIP Rationale

We intend TIPs to be the primary mechanisms for proposing new features, for collecting community technical input on an issue, and for documenting the design decisions that have gone into Tezos. Because the TIPs are maintained as text files in a versioned repository, their revision history is the historical record of the feature proposal.

For Tezos implementers, TIPs are a convenient way to track the progress of their implementation. Ideally each implementation maintainer would list the TIPs that they have implemented. This will give end users a convenient way to know the current status of a given implementation or library.

## TIP Types

There are three types of TIP:

- A **Standards Track TIP** describes any change that affects most or all Tezos implementations, such as a change to the the network protocol, a change in block or transaction validity rules, proposed application standards/conventions, or any change or addition that affects the interoperability of applications using Tezos. Furthermore Standard TIPs can be broken down into the following categories. Standards Track TIPs consist of a design document and an implementation.
  - **Protocol Upgrade** - improvements requiring a protocol upgrade.
  - **Networking** - improvements related to how nodes communicate.
  - **Interface** - improvements around client API/RPC specifications and standards.
  - **TRC** - application-level standards and conventions, including contract standards such as token standards, name registries, URI schemes, library/package formats, and wallet formats.
  - **Fork** - improvements requiring a hard fork.
- An **Informational TIP** describes a Tezos design issue, or provides general guidelines or information to the Tezos community, but does not propose a new feature. Informational TIPs do not necessarily represent Tezos community consensus or a recommendation, so users and implementers are free to ignore Informational TIPs or follow their advice.
- A **Meta TIP** describes a process surrounding Tezos or proposes a change to (or an event in) a process. Meta TIPs are like Standards Track TIPs but apply to areas other than the Tezos protocol itself. They may propose an implementation, but not to Tezos's codebase; they often require community consensus; unlike Informational TIPs, they are more than recommendations, and users are typically not free to ignore them. Examples include procedures, guidelines, changes to the decision-making process, and changes to the tools or environment used in Tezos development.

It is highly recommended that a single TIP contain a single key proposal or new idea. The more focused the TIP, the more successful it tends to be. A change to one client doesn't require a TIP; a change that affects multiple clients, or defines a standard for multiple apps to use, does.

A TIP must meet certain minimum criteria. It must be a clear and complete description of the proposed enhancement. The enhancement must represent a net improvement. The proposed implementation, if applicable, must be solid and must not complicate the protocol unduly.

## TIP Work Flow

Parties involved in the process are you, the champion or *TIP author*, the [*TIP editors*](#tip-editors).

:warning: Before you begin, vet your idea, this will save you time. Ask the Tezos community first if an idea is original to avoid wasting time on something that will be be rejected based on prior research (searching the Internet does not always do the trick). It also helps to make sure the idea is applicable to the entire community and not just the author. Just because an idea sounds good to the author does not mean it will work for most people in most areas where Tezos is used. Examples of appropriate public forums to gauge interest around your TIP include [the Tezos subreddit], [the Issues section of this repository], and [Tezos room on matrix] or [#tezos room on IRC]. In particular, [the Issues section of this repository] is an excellent place to discuss your proposal with the community and start creating more formalized language around your TIP.

Your role as the champion is to write the TIP using the style and format described below, shepherd the discussions in the appropriate forums, and build community consensus around the idea. Following is the process that a successful TIP will move along:

```
[ WIP ] -> [ DRAFT ] -> [ LAST CALL ] -> [ FINAL ]
```

Each status change is requested by the TIP author and reviewed by the TIP editors. Use a pull request to update the status. Please include a link to where people should continue discussing your TIP. The TIP editors will process these requests as per the conditions below.

* **Active** -- Some Informational and Process TIPs may also have a status of “Active” if they are never meant to be completed. E.g. TIP 1 (this TIP).
* **Work in progress (WIP)** -- Once the champion has asked the Tezos community whether an idea has any chance of support, they will write a draft TIP as a [pull request]. Consider including an implementation if this will aid people in studying the TIP.
  * :arrow_right: Draft -- If agreeable, TIP editor will assign the TIP a number (generally the issue or PR number related to the TIP) and merge your pull request. The TIP editor will not unreasonably deny a TIP.
  * :x: Draft -- Reasons for denying draft status include being too unfocused, too broad, duplication of effort, being technically unsound, not providing proper motivation or addressing backwards compatibility.
* **Draft** -- Once the first draft has been merged, you may submit follow-up pull requests with further changes to your draft until such point as you believe the TIP to be mature and ready to proceed to the next status. A TIP in draft status must be implemented to be considered for promotion to the next status.
  * :arrow_right: Final -- Proposals that have been successfully implemented and are widely used can be moved to Final.
  * :arrow_right: Proposed (Protocol Upgrade TIPs only) -- A finalized draft of a protocol upgrade will move to Proposed status, until the on-chain approval vote takes place.
* **Final** -- This TIP represents the current state-of-the-art. A Final TIP should only be updated to correct errata.

Protocol Upgrade TIPs have a special set of statuses that reflects their progress in the on-chain vote mechanism:

```
[ WIP ] -> [ DRAFT ] -> [ PROPOSED ] -> [ TESTING VOTE ] -> [ TESTING ] -> [ PROMOTION VOTE ] -> [ ACCEPTED ]
```
* **Proposed** -- A TIP has passed the approval vote and will be subject to more votes, using the Tezos protocol upgrade system.
  * :arrow_right: Testing Vote -- Shortly before the vote, the TIP will be moved to Testing Vote status and featured prominently.
* **Testing Vote** -- A vote about this TIP is imminent, using the Tezos protocol upgrade system. Proposals subject to a vote will be listed prominently.
  * :arrow_right: Testing -- A proposal that passed the first on-chain vote will enter a Testing phase.
  * :arrow_right: Draft -- A proposal that fails to pass the vote will be moved back to Draft.
* **Testing** -- The TIP is now running on a testnet to make sure it works as intended.
  * :arrow_right: Promotion Vote -- A second on-chain vote will take place after the testing period (see `amendment.ml` in the Tezos OCaml code for details).
* **Promotion Vote** -- The proposal has been running on a testnet for a while and a vote about it is imminent. Like with the Testing vote, the proposal will be listed prominently.
  * :arrow_right: Accepted -- A proposal that passed the on-chain vote will become Accepted.
  * :arrow_right: Draft -- A proposal that fails to pass the vote will be moved back to Draft.
* **Accepted** -- This TIP has passed the on-chain voting procedure and is now included in the protocol.

Other exceptional statuses include:

* Deferred -- This is for Fork TIPs that have been put off for a future hard fork.
* Rejected -- A TIP that is fundamentally broken. This is not intended for protocol upgrades that don't pass the on-chain vote.
* Active -- This is similar to Final, but denotes a TIP which which may be updated without changing its TIP number.
* Superseded -- A TIP which was previously final but is no longer considered state-of-the-art. Another TIP will be in Final status and reference the Superseded TIP.

## What belongs in a successful TIP?

Each TIP should have the following parts:

- Preamble - RFC 822 style headers containing metadata about the TIP, including the TIP number, a short descriptive title (limited to a maximum of 44 characters), and the author details. See [below](#tip-header-preamble) for details.
- Simple Summary - “If you can’t explain it simply, you don’t understand it well enough.” Provide a simplified and layman-accessible explanation of the TIP.
- Abstract - a short (~200 word) description of the technical issue being addressed.
- Motivation (*optional) - The motivation is critical for TIPs that want to change the Tezos protocol. It should clearly explain why the existing protocol specification is inadequate to address the problem that the TIP solves. TIP submissions without sufficient motivation may be rejected outright.
- Specification - The technical specification should describe the syntax and semantics of any new feature. The specification should be detailed enough to allow competing, interoperable implementations for any of the current Tezos platforms.
- Rationale - The rationale fleshes out the specification by describing what motivated the design and why particular design decisions were made. It should describe alternate designs that were considered and related work, e.g. how the feature is supported in other languages. The rationale may also provide evidence of consensus within the community, and should discuss important objections or concerns raised during discussion.
- Backwards Compatibility - All TIPs that introduce backwards incompatibilities must include a section describing these incompatibilities and their severity. The TIP must explain how the author proposes to deal with these incompatibilities. TIP submissions without a sufficient backwards compatibility treatise may be rejected outright.
- Test Cases - Test cases for an implementation are mandatory for TIPs that are affecting consensus changes. Other TIPs can choose to include links to test cases if applicable.
- Implementations - The implementations must be completed before any TIP is given status “Final”, but it need not be completed before the TIP is merged as draft. While there is merit to the approach of reaching consensus on the specification and rationale before writing code, the principle of “rough consensus and running code” is still useful when it comes to resolving many discussions of API details.
- Copyright Waiver - All TIPs must be in the public domain. See the bottom of this TIP for an example copyright waiver.

## TIP Formats and Templates

TIPs should be written in [markdown] format.
Image files should be included in a subdirectory of the `assets` folder for that TIP as follow: `assets/tip-X` (for tip **X**). When linking to an image in the TIP, use relative links such as `../assets/tip-X/image.png`.

## TIP Header Preamble

Each TIP must begin with an RFC 822 style header preamble, preceded and followed by three hyphens (`---`). The headers must appear in the following order. Headers marked with "*" are optional and are described below. All other headers are required.

` tip:` \<TIP number\> (this is determined by the TIP editor)

` title:` \<TIP title\>

` author:` \<a list of the author's or authors' name(s) and/or username(s), or name(s) and email(s). Details are below.\>

` * discussions-to:` \<a url pointing to the official discussion thread\>

 - :x: `discussions-to` can not be a `Merge Request`.

` status:` | Draft | Last Call | Vote | Accepted | Final | Active | Deferred | Rejected | Superseded |

` * review-period-end:` YYYY-MM-DD

` type:` | Standards Track (Protocol Upgrade, Networking, Interface, TRC, Fork) | Informational | Meta |

` * category:` | Protocol Upgrade | Networking | Interface | TRC | Fork |

` created:` \<date created on, in ISO 8601 (yyyy-mm-dd) format\>

` * requires:` \<TIP number(s)\>

` * replaces:` \<TIP number(s)\>

` * superseded-by:` \<TIP number(s)\>

` * resolution:` \<a url pointing to the resolution of this TIP\>

#### Author header

The author header optionally lists the names, email addresses or usernames of the authors/owners of the TIP. Those who prefer anonymity may use a username only, or a first name and a username. The format of the author header value must be:

Random J. User &lt;address@dom.ain&gt;

or

Random J. User (@username)

if the email address or GitHub username is included, and

Random J. User

if the email address is not given.

Note: The resolution header is required for Standards Track TIPs only. It contains a URL that should point to an email message or other web resource where the pronouncement about the TIP is made.

While a TIP is a draft, a discussions-to header will indicate the mailing list or URL where the TIP is being discussed. As mentioned above, examples for places to discuss your TIP include [Tezos room on matrix], [#tezos room on IRC] an issue in this repo or in a fork of this repo, [Tezos Developer Community Google Group](https://groups.google.com/forum/#!forum/tezos-developer-community), and [Reddit r/tezos](https://www.reddit.com/r/tezos/). No discussions-to header is necessary if the TIP is being discussed privately with the author.

The type header specifies the type of TIP: Standards Track, Meta, or Informational. If the track is Standards please include the subcategory (protocol upgrade, fork, networking, interface, or TRC).

The category header specifies the TIP's category. This is required for standards-track TIPs only.

The created header records the date that the TIP was assigned a number. Both headers should be in yyyy-mm-dd format, e.g. 2001-08-14.

TIPs may have a requires header, indicating the TIP numbers that this TIP depends on.

TIPs may also have a superseded-by header indicating that a TIP has been rendered obsolete by a later document; the value is the number of the TIP that replaces the current document. The newer TIP must have a Replaces header containing the number of the TIP that it rendered obsolete.

Headers that permit lists must separate elements with commas.

## Auxiliary Files

TIPs may include auxiliary files such as diagrams. Such files must be named TIP-XXXX-Y.ext, where “XXXX” is the TIP number, “Y” is a serial number (starting at 1), and “ext” is replaced by the actual file extension (e.g. “png”).

## Transferring TIP Ownership

It occasionally becomes necessary to transfer ownership of TIPs to a new champion. In general, we'd like to retain the original author as a co-author of the transferred TIP, but that's really up to the original author. A good reason to transfer ownership is because the original author no longer has the time or interest in updating it or following through with the TIP process, or has fallen off the face of the 'net (i.e. is unreachable or isn't responding to email). A bad reason to transfer ownership is because you don't agree with the direction of the TIP. We try to build consensus around a TIP, but if that's not possible, you can always submit a competing TIP.

If you are interested in assuming ownership of a TIP, send a message asking to take over, addressed to both the original author and the TIP editor. If the original author doesn't respond to email in a timely manner, the TIP editor will make a unilateral decision (it's not like such decisions can't be reversed :)).

## TIP Editors

The current TIP editors are

* Martin Pospěch (@thisandthat)
* Fred Yankowski (@fredcy)
* Rauan Mayemir (@rauanmayemir)


## TIP Editor Responsibilities

For each new TIP that comes in, an editor does the following:

- Read the TIP to check if it is ready: sound and complete. The ideas must make technical sense, even if they don't seem likely to get to final status.
- The title should accurately describe the content.
- Check the TIP for language (spelling, grammar, sentence structure, etc.), markup (Github flavored Markdown), code style.

If the TIP isn't ready, the editor will send it back to the author for revision, with specific instructions.

Once the TIP is ready for the repository, the TIP editor will:

- Assign a TIP number (generally the PR number or, if preferred by the author, the Issue # if there was discussion in the Issues section of this repository about this TIP).
- Merge the corresponding pull request.
- Send a message back to the TIP author with the next step.

Many TIPs are written and maintained by developers of Tezos implementations or ecosystem projects. The TIP editors monitor TIP changes, and correct any structure, grammar, spelling, or markup mistakes we see.

The editors don't pass judgment on TIPs. We merely do the administrative & editorial part.

## History

This document was derived heavily from [Ethereum's EIP-1] written by Martin Becze, Hudson Jameson, and others which in turn was derived from [Bitcoin's BIP-0001] and [Python's PEP-0001]. In many places text was simply copied and modified. Although the PEP-0001 text was written by Barry Warsaw, Jeremy Hylton, and David Goodger, they are not responsible for its use in the Tezos Improvement Process, and should not be bothered with technical questions specific to Tezos or the TIP. Please direct all comments to the TIP editors.

August 15, 2018: TIP 1 has been created and will be placed as a PR.

See [the revision history for further details](https://gitlab.com/tips2/TIPs/commits/master/TIPS/tip-1.md), which is also available by clicking on the History button in the top right of the TIP.

### Bibliography

[the Tezos subreddit]: https://www.reddit.com/r/tezos/
[Tezos room on matrix]: https://riot.im/app/#/room/#tezos:matrix.org
[#tezos room on IRC]: https://riot.im/app/#/room/#freenode_#tezos:matrix.org
[pull request]: https://gitlab.com/tips2/TIPs/merge_requests
[the Issues section of this repository]: https://gitlab.com/tips2/TIPs/issues
[markdown]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[Ethereum's EIP-1]: https://github.com/ethereum/EIPs
[Bitcoin's BIP-0001]: https://github.com/bitcoin/bips
[Python's PEP-0001]: https://www.python.org/dev/peps/

## Copyright

Copyright and related rights waived via [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
