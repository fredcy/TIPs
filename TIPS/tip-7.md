---
tip: 7
title: TRC-7 Tezos ICO Standard
status: Draft
type: Standards Track
category: TRC
author: Martin Pospěch <martin@pospech.cz>
discussions-to: https://gitlab.com/tips2/TIPs/issues/7
created: 2018-09-19
---

## Simple Summary

A standard interface for ICO tokens.

## Abstract

This document describes the interface for Tezos ICO smart contracts written in [Liquidity](http://liquidity-lang.org). The standard provides basic functionality to transfer tokens, as well as allow tokens to be approved so they can be spent by another on-chain third party. Inspiration has been drawn from the widely used Ethereum standard ERC-20.

## Motivation

Using a common interface will make it easier for exchanges and wallets to interact with such contract, improving adoption. Liquidity was chosen as it seems to be more accessible to developers than Michelson.

## Specification

The basic functionality that all ICO contracts should implement:

1. Participate -- participate in the ICO
2. Transfer -- transfer tokens to another address within the ICO contract
3. TransferFrom -- draw tokens from another address if allowed
4. Approve -- set allowance for another address
5. Balance -- retrieve balance of an address
6. Allowance -- retrieve allowance for a source-destination address pair
7. Supply -- retrieve token supply
8. Placeholder -- to help with future compatibility

Balance, Allowance and Supply let contracts query the ICO contract -- this requires the caller to provide a contract that will receive the callback.

In Liquidity this could look like the following:

    (* s not shown for simplicity *)
    
    type p =
      | Participate of unit
      | Transfer of address * tez
      | TransferFrom of address * tez
      | Approve of address * tez
      | Balance of address * tez contract
    (* Allowance is a tuple of source, destination and callback contract, in that order *)
      | Allowance of (address * address) * tez contract
      | Supply of tez contract
      | Placeholder of unit
    
    let%entry main
        (parameter : p)
        (storage : s)
      : operation list * s =

It may look like we can access different functions of a contract by names, like in Solidity, but this is not the case. The Liquidity parameter shown above compiles to the following Michelson:

    parameter
      (or
        unit
        (or
          (pair
            address
            mutez)
          (or
            (pair
              address
              mutez)
            (or
              (pair
                address
                mutez)
              (or
                (pair
                  address
                  (contract
                    mutez))
                (or
                  (pair
                    (pair
                      address
                      address)
                    (contract
                      mutez))
                  (or
                    (contract
                      mutez)
                    unit)))))));

Variant types in Liquidity compile to a tree composed of `or` types. You can see that different inputs are recognized by their position within the tree, rather than a name. The first variant is accessed by calling with the argument `Left x`, the second with `Right (Left y)`, and so forth. Adding more variants adds to the end of the tree.

Implications for ICO contracts:

1. Use the same order of variants as shown above.
2. Any additional types need to come after the types for basic functionality.
3. The best "slot" is granted to the contribution action, as it will be used by wallets during the ICO, and possibly entered manually by users of command line clients.
4. The other slots are ordered by expected usage frequency.
5. Compatibility with future extensions requires that a placeholder be added as a last item of the parameter type. Future extensions should replace the placeholder with their own type and add their own placeholder as a last item.

## Implementation considerations

### Authentication
The contract relies on the Tezos key system to assess ownership. Addresses in the ICO contract correspond to regular Tezos addresses -- that is, hashes of Tezos public keys (starting `tz`) and contract addresses (starting with `KT1`). Anyone able to transfer from an address is assumed to be its owner.

ICO contracts should use `SENDER` (`Current.sender`) to identify callers. `SOURCE` could allow callers to implement more complicated logic on their end, but would allow users to be tricked into transferring to malicious contracts that then steal their ICO tokens. Compatibility with multi-sig wallets also requires using `SENDER`.

### Wallet UX
When presenting a transaction to a user for approval, wallets should present the parameter in a readable format (such as the S-expression format). To avoid human caused errors, users should not be allowed to edit the parameter. An advanced option can be provided to enable editing. A payment request format is going to be discussed as a separate proposal.

### Integration stories
To explore the stregths and limitations of this proposal, we'll look at several likely scenarios.

#### Wallet trying to add support for compliant ICO contract
To make sure a contract is compliant, the wallet should query the blockchain for the contract's parameter type. It should be compared with the parameter type presented above (converted to the JSON RPC format). If all types are equal, the contract is assumed to be compliant. This could go wrong with contracts that use an identical parameter type but are not actually compatible. In the future, Liquidity types are going to compile to Michelson types with annotations, which will let us impose a stricter compatibility check.

Depending on the functionality ("method") being used, the wallet will wrap the parameter as seen in the following table:

Action | Argument
---|---
Participate|`Left Unit`
Transfer|`Right (Left (Pair "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR" 123456))`

<br>
When interacting with the Tezos node RPC the S-expressions (used e.g. by tezos-client) will have to be converted to the JSON format:

    // Participate in the ICO
    {
        "prim": "Left",
        "args": [
            {
                "prim": "Unit",
                "args": []
            }
        ]
    }
    
    // Transfer
    {
        "prim": "Right",
        "args": [
            {
                "prim": "Left",
                "args": [
                    {
                        "prim": "Pair",
                        "args": [
                            {
                                "string": "tz1Ph8mdwaRp71XvixaExcNKtPvQshe5BwcR"
                            },
                            {
                                "int": "123456"
                            }
                        ]
                    }
                ]
            }
        ]
    }

#### Compliant contract with extended functionality
A contract creator writes a contract that implements the basic functionality and some additional "methods". The simple compatibility check described in the previous scenario will fail. We can, however, check that the whole type is compatible except the placeholder, which is replaced by another type in the extended contract.

The wallet can easily offer the basic functionality for the extended contract with zero changes, since it just needs to wrap the parameter as usual. The extended functionality won't be available unless explicitly implemented by the wallet.

#### Non-compliant contract
If both the simple and the extended compatibility check fail, the contract is incompatible with the standard and has to be supported separately. Accidental transfers to such contract, assuming parameters wrapped as described above should fail at simulation.

#### Smart contract caller
A contract implementing the basic functionality can interact with other smart contracts easily. Contracts with extended functionality will have a differing type and fail at runtime. To work with the extended functionality contract, the caller has to know the type and support it explicitly. This is one of the important limitations of this proposal -- the standard is meant to ease developer coordination, rather than ensure inter-contract compatibility.