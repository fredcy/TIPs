# TIPs
Tezos Improvement Proposals (TIPs) describe standards for the Tezos platform, including core protocol specifications, client APIs, and contract standards.

An overview of TIPs can be found [here](overview.md).

# Contributing

 1. Review [TIP-1](TIPS/tip-1.md).
 2. Fork the repository by clicking "Fork" in the top right.
 3. Create a new branch for your TIP in your fork the repository. There is a [template TIP here](tip-X.md).
 4. Submit a Pull Request to Tezos's [TIPs repository](https://gitlab.com/tips2/TIPs/).

Your first PR should be a first draft of the final TIP. It must meet the formatting criteria enforced by the build (largely, correct metadata in the header). An editor will manually review the first PR for a new TIP and assign it a number before merging it. Make sure you include a `discussions-to` header with the URL to a discussion forum or open GitHub issue where people can discuss the TIP as a whole.

If your TIP requires images, the image files should be included in a subdirectory of the `assets` folder for that TIP as follow: `assets/tip-X` (for tip **X**). When linking to an image in the TIP, use relative links such as `../assets/tip-X/image.png`.

When you believe your TIP is mature and ready to progress past the draft phase, you should open a PR changing the state of your TIP to 'Final'. An editor will review your draft and ask if anyone objects to its being finalised. If the editor decides there is no rough consensus - for instance, because contributors point out significant issues with the TIP - they may close the PR and request that you fix the issues in the draft before trying again.

# TIP Status Terms
* **Draft** - A TIP that is undergoing rapid iteration and changes.
* **Final** - A TIP that has been thoroughly discussed, is widely used and all concerns about it have been addressed can become Final.
* **Proposed** - A TIP that has been proposed through the on-chain protocol upgrade mechanism, and will be subject to more votes.
* **Testing** - A TIP that has passed a testing vote and is running on a testnet to make sure it works as intended.
* **Accepted** - A protocol upgrade TIP that has passed an on-chain vote and is now included in the protocol.

# Preferred Citation Format

The canonical URL for a TIP that has achieved draft status at any point is at https://gitlab.com/tips2/TIPs/blob/master/TIPS/tip-X.md. For example, the canonical URL for TIP-1 is https://gitlab.com/tips2/TIPs/blob/master/TIPS/tip-1.md.
