
ATTENTION! If you would like to submit a TIP and it has already been written as a draft (see the [template] for an example), please submit it as a [merge request].

If you are considering a proposal but would like to get some feedback on the idea before submitting a draft, then continue opening an Issue as a thread for discussion.  Note that the more clearly and completely you state your idea the higher the quality of the feedback you are likely to receive.

Keep in mind the following guidelines from [TIP-1]:

> Each TIP must have a champion - someone who writes the TIP using the style and format described below, shepherds the discussions in the appropriate forums, and attempts to build community consensus around the idea. The TIP champion (a.k.a. Author) should first attempt to ascertain whether the idea is TIP-able. Posting to the the Protocol Discussion forum or opening an Issue is the best way to go about this.

> Before you begin, vet your idea, this will save you time. Ask the Tezos community first if an idea is original to avoid wasting time on something that will be be rejected based on prior research (searching the Internet does not always do the trick). It also helps to make sure the idea is applicable to the entire community and not just the author. Just because an idea sounds good to the author does not mean it will work for most people in most areas where Tezos is used. Examples of appropriate public forums to gauge interest around your TIP include [the Tezos subreddit], [the Issues section of this repository], and [Tezos room on matrix] or [#tezos room on IRC]. In particular, [the Issues section of this repository] is an excellent place to discuss your proposal with the community and start creating more formalized language around your TIP.

> Once the champion has asked the Tezos community whether an idea has any chance of support, they will write a draft TIP as a [merge request]. This gives the author a chance to flesh out the draft TIP to make properly formatted, of high quality, and to address initial concerns about the proposal. Consider including an implementation if this will aid people in studying the TIP. 

<!-- Use the Preview for clickable links. -->
[template]: (https://gitlab.com/tips2/TIPs/blob/master/tip-X.md)
[merge request]: https://gitlab.com/tips2/TIPs/merge_requests
[TIP-1]: https://gitlab.com/tips2/TIPs/blob/master/TIPS/tip-1.md
[the Tezos subreddit]: https://www.reddit.com/r/tezos/
[the Issues section of this repository]: https://gitlab.com/tips2/TIPs/issues
[Tezos room on matrix]: https://riot.im/app/#/room/#tezos:matrix.org
[#tezos room on IRC]: https://riot.im/app/#/room/#freenode_#tezos:matrix.org